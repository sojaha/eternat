# eternat

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Suivi du projet

### Etape 0: Liaison du projet Firebase (fait)
--- 
- Création d'un nouveau projet sur Firebase
- Création d'une nouvelle application Android
- Ajout de google-services.json dans android/app
- Modification des fichiers build.gradle
- ça compile ? Oui alors passons à l'étape 1

### Etape 1: Mise en place d'une page authentification (fait)
---
 - Création d'un helper qui va gérer les différentes authentification (signup/login/logout)
 - Création d'un dossier __authentification__ qui va permettre l'authentification

### Etape 2: Mise en place d'un système de rôle (fait)
- Création d'un dossier educator et d'un dossier student. Mon but en faisant celà, c'est de créer une interface spécifique pour les éducateur et une autre pour les étudiants.
> La partie educator sera le plus gros du projet car il faudra mettre en place un système d'ajout d'élèves.
- Création d'une mainpage, celà va servir à savoir quel rôle on a lors de l'authentification

### Etape 3: Mise en place d'un splashScreen (fait à moitié)
- Dossier splashscreen en place mais pour le moment, c'est une fonctionnalité faite de manière "manuelle". Il faudra l'automatiser avec la possibilité de pouvoir lier notre firebase et le temps d'authentification.

### Etape 4: Création du squelette de notre page d'educateur (en cours)
- La P

### Etape 5: Création de profil
- Ajout de la dépendance image_picker dans le pubspec.yaml (https://pub.dev/packages/image_picker), Il faudra ensuite rajouter le nécessaire pour IOS
- Pour enregistrer mon image, je vais utiliser firebase_storage
- DateTime ==> On va changer les fonctions de géolocalisation afin d'avoir le tout en français
- Map Statique: il faut installer location dans les pubspecs; ensuite, il faudra suivre ce que le readme de la librairie demande afin de configurer l'appareil correctement https://pub.dev/packages/location
> Normalement avec Flutter 1.12, toutes les dépendencies sont automatiquement ajoutées dans le projet.
- Map Dynamique: on va pouvoir ajouter google_maps_flutter dans le pubspec.yaml. Cette librairie est gérée directement par Flutter Teams
> Pour faire un get de la localisation, je vais utiliser de l'http, donc rajouter la librairie http dans le pubspec
>> WARNING ! J'ai pas terminé de faire ma petite fonction de map, du coup, je me contenterais juste de faire une recherche avec autocomplétion.
