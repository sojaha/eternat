import 'package:eternat/student/widgets/student_home_form.dart';
import 'package:flutter/material.dart';

class StudentHomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('StudentHomePage'),
      ),
      body: StudentHomeForm(),
    );
  }
}
