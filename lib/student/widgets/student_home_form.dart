import 'package:eternat/authentication/auth/screens/auth_screen.dart';
import 'package:eternat/services/firebaseauth_service.dart';
import 'package:flutter/material.dart';
import 'package:lite_rolling_switch/lite_rolling_switch.dart';

class StudentHomeForm extends StatefulWidget {
  @override
  _StudentHomeFormState createState() => _StudentHomeFormState();
}

class _StudentHomeFormState extends State<StudentHomeForm> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("StudentHomePage"),
          LiteRollingSwitch(
            value: true,
            textOn: "On",
            textOff: "Off",
            colorOn: Colors.greenAccent,
            colorOff: Colors.redAccent,
            iconOn: Icons.airline_seat_individual_suite_rounded,
            iconOff: Icons.home,
            textSize: 18.0,
            onChanged: (bool position) {

            },
          ),
          RaisedButton(
            child: Text("Log out"),
            onPressed: () {
              FirebaseAuthService.logOut();
              Route route = MaterialPageRoute(builder: (ctx) => AuthScreen());
              Navigator.pushReplacement(context, route);
            },
          )
        ],
      ),
    );
  }
}
