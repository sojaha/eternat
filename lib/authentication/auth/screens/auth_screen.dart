import 'dart:ui';

import 'package:eternat/authentication/auth/widgets/auth_form.dart';
import 'package:eternat/services/authentication_service.dart';
import 'package:eternat/services/firebaseauth_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  //fonction qui permet d'obtenir toutes les données du formulaire hors ligne
  //comme paramètres et donc comme entrées
  void _submitAuthForm(
    String email,
    String password,
    BuildContext ctx,
  ) async {
    //Avant la version 0.18, c'était AuthResult
    try {
      final user = await FirebaseAuthService.signInWithEmailAndPassword(
          email: email, password: password);
      if (user != null) {
        print("login successful");
        AuthenticationService.connection(context);
      }
    } on PlatformException catch (err) {
      var message =
          "Une erreur à été produite, s'il vous plait veuillez vérifier vos identifiants.";
      if (err.message != null) {
        message = err.message;
      }
      Scaffold.of(ctx).showSnackBar(
        SnackBar(
          content: Text(message),
          backgroundColor: Theme.of(context).errorColor,
        ),
      );
    } catch (err) {
      print(err);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/background.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            body: AuthForm(
              _submitAuthForm,
            ),
          ),
        ),
      ),
    );
  }
}
