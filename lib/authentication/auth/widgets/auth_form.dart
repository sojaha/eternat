import 'package:eternat/educator/screens/create_student_screen.dart';
import 'package:flutter/material.dart';

class AuthForm extends StatefulWidget {
  AuthForm(this.submitFn);

  final void Function(
    String email,
    String password,
    BuildContext context,
  ) submitFn; //submitFn pour submitFonction

  @override
  _AuthFormState createState() => _AuthFormState();
}

class _AuthFormState extends State<AuthForm> {
  //Clé qui sera reliée au formulaire
  final _formKey = GlobalKey<FormState>();
  var _userEmail = '';
  var _userPassword = '';

  //Fonction qui essaiera de soumettre la clé pour valider le formulaire
  void _trySubmit() {
    //On demande l'état actuel du formulaire et on le valide
    final isValid = _formKey.currentState.validate();
    //Fermer le clavier programmable qui pourrait être ouvert dès que l'on soumet le formulaire
    FocusScope.of(context).unfocus();
    //Si c'est valide, alors on va sauvegarder l'état du formulaire
    if (isValid) {
      _formKey.currentState.save();
      widget.submitFn(
        //trim pour supprimer tous les espaces blancs entre le début et la fin
        _userEmail.trim(),
        _userPassword.trim(),
        context,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Card(
          margin: EdgeInsets.all(20),
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.all(16),
              child: Form(
                key:
                    _formKey, //Fixe l'argument clé du formulaire en tant que _formKey
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextFormField(
                      key: ValueKey('email'),
                      validator: (value) {
                        if (value.isEmpty || !value.contains('@insuptou.be')) {
                          return "Entrez une adresse mail valide (@insuptou.be)";
                        }
                      },
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        labelText: 'Adresse E-mail',
                        suffixIcon: Icon(Icons.email),
                      ),
                      onSaved: (value) {
                        //quand c'est valide, on prend la value et on la stocke en tant que _userEmail
                        _userEmail = value;
                      },
                    ),
                    TextFormField(
                      key: ValueKey('password'),
                      validator: (value) {
                        if (value.isEmpty) {
                          return "mot de passe incorect";
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        labelText: 'Mot de passe',
                        suffixIcon: Icon(Icons.lock),
                      ),
                      obscureText: true, //On va cacher le texte
                      onSaved: (value) {
                        //quand c'est valide, on prend la value et on la stocke en tant que _userPassword
                        _userPassword = value;
                      },
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    RaisedButton(
                      child: Text('Se connecter'),
                      color: Colors.red,
                      textColor: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      onPressed:
                          _trySubmit, //Appel de la fonction sans parenthèse car nous voulons
                      //juste pointer pour qu'il soit exécuté en notre nom
                    ),
                    RaisedButton(
                      child: Text("Create Account"),
                      onPressed: () async {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => CreateStudentScreen(),
                          ),
                        );
                      },
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
