import 'package:eternat/educator/models/place.dart';
import 'package:eternat/helpers/location_helper.dart';
import 'package:flutter/foundation.dart';

class PlaceProvider with ChangeNotifier {
  Place place;

  Future<void> updatePlace(PlaceLocation pickedLocation) async {
    final address = await LocationHelper.getPlaceAddress(pickedLocation.latitude, pickedLocation.longitude);
    final updatedLocation = PlaceLocation(latitude: pickedLocation.latitude, longitude: pickedLocation.longitude, address: address);
    place = Place(location: updatedLocation);
    notifyListeners();
  }
}