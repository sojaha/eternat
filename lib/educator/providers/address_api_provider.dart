import 'dart:convert';
import 'dart:io';

import 'package:eternat/services/address_service.dart';
import 'package:http/http.dart';

class AddressApiProvider {
  final client = Client();
  final sessionToken;

  AddressApiProvider(this.sessionToken);


  static final String androidKey = 'AIzaSyAdIHdp-e6Nn9MbIP9K4NqE0rcyR1XIbEc';
  static final String iosKey = 'YOUR_API_KEY_HERE';
  final apiKey = Platform.isAndroid ? androidKey : iosKey;

  Future<List<Suggestion>> fetchSuggestions(String input, String lang) async {
    final request =
        'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$input&types=address&language=$lang&components=country:be|country:fr&key=$apiKey&sessiontoken=$sessionToken';
    final response = await client.get(request);

    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      if (result['status'] == 'OK') {
        // compose suggestions in a list
        return result['predictions']
            .map<Suggestion>((p) => Suggestion(p['place_id'], p['description']))
            .toList();
      }
      if (result['status'] == 'ZERO_RESULTS') {
        return [];
      }
      throw Exception(result['error_message']);
    } else {
      throw Exception('Failed to fetch suggestion');
    }
  }

  Future<Address> getPlaceDetailFromId(String placeId) async {
    // if you want to get the details of the selected place by place_id
    final request =
        'https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeId&fields=address_component&key=$apiKey&sessiontoken=$sessionToken';
    final response = await client.get(request);

    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      if (result['status'] == 'OK') {
        final components =
        result['result']['address_components'] as List<dynamic>;
        // build result
        final address = Address();
        components.forEach((c) {
          final List type = c['types'];
          if (type.contains('street_number')) {
            address.streetNumber = c['long_name'];
          }
          if (type.contains('route')) {
            address.street = c['long_name'];
          }
          if (type.contains('locality')) {
            address.city = c['long_name'];
          }
          if (type.contains('postal_code')) {
            address.postalCode = c['long_name'];
          }
        });
        return address;
      }
      throw Exception(result['error_message']);
    } else {
      throw Exception('Failed to fetch suggestion');
    }
  }
}
