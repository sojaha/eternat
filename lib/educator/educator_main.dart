import 'package:eternat/educator/screens/create_student_screen.dart';
import 'package:eternat/educator/screens/educator_home_screen.dart';
import 'package:eternat/educator/screens/student_overview_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

class EducatorMain extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'E-ternat',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        accentColor: Colors.red[700],
      ),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('fr', 'FR'),
      ],
      home: EducatorHomeScreen(),
      routes: {
        StudentOverviewScreen.routeName: (ctx) => StudentOverviewScreen(),
        CreateStudentScreen.routeName: (ctx) => CreateStudentScreen(),
      },
    );
  }
}