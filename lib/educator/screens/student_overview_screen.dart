import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:eternat/educator/widgets/educator_drawer.dart';
import 'package:eternat/educator/widgets/student_item.dart';
import 'package:flutter/material.dart';

class StudentOverviewScreen extends StatelessWidget {
  static const routeName = '/student_overview';

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: FirebaseFirestore.instance.collection('users').snapshots(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.data == null)
          return Material(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        return FutureBuilder(
          future: FirebaseFirestore.instance
              .collection('users')
              .where('role', isEqualTo: 'étudiant')
              .get(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null)
              return Material(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            return Scaffold(
              appBar: AppBar(
                title: Text('Liste des étudiants'),
              ),
              drawer: EducatorDrawer(),
              body: GridView.builder(
                padding: const EdgeInsets.all(10.0),
                itemCount: snapshot.data.docs.length,
                itemBuilder: (ctx, i) {
                  return StudentItem(
                    snapshot.data.docs[i].get('id'),
                    snapshot.data.docs[i].get('nom'),
                    snapshot.data.docs[i].get('prénom'),
                    snapshot.data.docs[i].get('imageUrl'),
                    snapshot.data.docs[i].get('isDislodge'),
                    snapshot.data.docs[i].get('isBack'),
                  );
                },
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 3 / 2,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                ),
              ),
            );
          },
        );
      },
    );
  }
}
