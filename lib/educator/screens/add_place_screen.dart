import 'package:eternat/educator/models/place.dart';
import 'package:eternat/educator/providers/place_provider.dart';
import 'package:eternat/educator/widgets/location_input.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddPlaceScreen extends StatefulWidget {
  @override
  _AddPlaceScreenState createState() => _AddPlaceScreenState();
}

class _AddPlaceScreenState extends State<AddPlaceScreen> {
  PlaceLocation _pickedLocation;

  void _selectPlace(double lat, double lng) {
    _pickedLocation = PlaceLocation(latitude: lat, longitude: lng);
  }

  void _savePlace() {
    if (_pickedLocation == null) {
      return;
    }
    Provider.of<PlaceProvider>(context, listen: false).updatePlace(_pickedLocation);
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ajouter une adresse'),
      ),
      resizeToAvoidBottomPadding: false,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch, //Etire le bouton
        children: [
          //On peut se débarrasser de l'alignement de l'axe principal grâce à l'expanded
          //Le premier enfant (expanded) prendra dans cette colonne, toute la hauteur qu'il peut obtenir
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.all(10),
                child: Column(
                  children: [
                    LocationInput(_selectPlace),
                  ],
                ),
              ),
            ),
          ),
          RaisedButton.icon(
            onPressed: _savePlace,
            icon: Icon(Icons.add),
            label: Text("Confirmer l'adresse"),
            elevation: 0,
            //Supprime l'ombre du bouton
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            //Se débarrasse de la marge supplémentaire autour du bouton
            color: Theme.of(context).accentColor,
            textColor: Colors.white,
          ),
        ],
      ),
    );
  }
}
