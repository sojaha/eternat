import 'package:eternat/educator/widgets/create_student_form.dart';
import 'package:eternat/educator/widgets/educator_drawer.dart';
import 'package:eternat/educator/widgets/pickers/user_image_picker.dart';
import 'package:flutter/material.dart';

class CreateStudentScreen extends StatelessWidget {
  static const routeName = "/create_student";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ajout d'utilisateur"),
      ),
      drawer: EducatorDrawer(),
      resizeToAvoidBottomPadding: false,
      body: CreateStudentForm(),
    );
  }
}
