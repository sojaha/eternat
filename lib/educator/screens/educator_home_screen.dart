import 'package:eternat/educator/widgets/educator_drawer.dart';
import 'package:flutter/material.dart';

class EducatorHomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page principale'),
      ),
      drawer: EducatorDrawer(),
    );
  }
}
