import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:date_format/date_format.dart';
import 'package:eternat/educator/utils/profile_clipper.dart';
import 'package:eternat/services/firebaseauth_service.dart';
import 'package:eternat/services/firebasefirestore_service.dart';
import 'package:flutter/material.dart';

class CheckUserScreen extends StatelessWidget {
  String _profileID;

  CheckUserScreen(String profileID) {
    this._profileID = profileID;
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: FirebaseFirestoreService.userCollection.snapshots(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.data == null)
          return Material(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        return FutureBuilder(
          future: FirebaseFirestoreService.getSpecificUser(_profileID),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null)
              return Material(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            Map<String, dynamic> data = snapshot.data.data();
            return Scaffold(
              appBar: AppBar(
                actions: [
                  IconButton(
                    icon: Icon(Icons.auto_delete),
                    onPressed: () async {
                      FirebaseAuthService.deleteUser(
                          data['email'],
                          data['password'],);
                    },
                  ),
                ],
              ),
              resizeToAvoidBottomInset: false,
              body: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      height: 300.0,
                      child: Stack(
                        children: [
                          ClipPath(
                            clipper: ProfileClipper(),
                            child: Container(
                              height: 300.0,
                              decoration: BoxDecoration(
                                color: data['isDislodge']
                                    ? Colors.grey
                                    : data['isBack']
                                        ? Colors.blue
                                        : Colors.red,
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment(0, -0.5),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                CircularProfileAvatar(
                                  data['imageUrl'],
                                  radius: 55,
                                  borderWidth: 4.0,
                                ),
                                const SizedBox(height: 4.0),
                                Text(
                                  "${data['nom']} ${data['prénom']}",
                                  style: TextStyle(
                                      fontSize: 21.0,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  "${data['role']}",
                                  style: TextStyle(
                                    fontSize: 12.0,
                                    color: Colors.grey[700],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Align(
                            alignment: Alignment(-0.75, 5),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Card(
                                  color: Theme.of(context).primaryColor,
                                  margin: EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 25.0),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(50)),
                                  child: ListTile(
                                    leading: Icon(
                                      Icons.date_range_sharp,
                                      color: Theme.of(context).accentColor,
                                    ),
                                    title: Text(
                                      formatDate(
                                        data['date_de_naissance'].toDate(),
                                        [yyyy, '-', mm, '-', dd],
                                      ),
                                      style: TextStyle(
                                          fontSize: 20.0, color: Colors.white),
                                    ),
                                  ),
                                ),
                                Card(
                                  color: Theme.of(context).primaryColor,
                                  margin: EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 25.0),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20)),
                                  child: Column(
                                    children: [
                                      ListTile(
                                        leading: Icon(
                                          Icons.location_city,
                                          color: Theme.of(context).accentColor,
                                        ),
                                        title: Text(
                                          "${data['adresse']['code_postal']}, ${data['adresse']['ville']}",
                                          style: TextStyle(
                                              fontSize: 20.0,
                                              color: Colors.white),
                                        ),
                                      ),
                                      ListTile(
                                        leading: Icon(
                                          Icons.date_range_sharp,
                                          color: Theme.of(context).accentColor,
                                        ),
                                        title: Text(
                                          "${data['adresse']['numéro']}, ${data['adresse']['rue']}",
                                          style: TextStyle(
                                              fontSize: 20.0,
                                              color: Colors.white),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }
}
