import 'package:eternat/educator/screens/create_student_screen.dart';
import 'package:eternat/educator/screens/student_overview_screen.dart';
import 'package:eternat/services/firebaseauth_service.dart';
import 'package:flutter/material.dart';

class EducatorDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          AppBar(title: Text('Eternat'),
          automaticallyImplyLeading: false,
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.assignment_ind_outlined),
            title: Text('Page principale'),
            onTap: () {
              Navigator.of(context).pushReplacementNamed('/');
            },
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.assignment_ind_outlined),
            title: Text('Liste des étudiants'),
            onTap: () {
              Navigator.of(context).pushReplacementNamed(StudentOverviewScreen.routeName);
            },
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.add),
            title: Text('Ajouter un étudiant'),
            onTap: () {
              Navigator.of(context).pushReplacementNamed(CreateStudentScreen.routeName);
            },
          ),
          RaisedButton(
            child: Text("Log out"),
            onPressed: () {
              FirebaseAuthService.logOut();
            },
          ),
        ],
      ),
    );
  }
}
