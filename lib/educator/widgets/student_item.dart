import 'package:eternat/educator/screens/check_user_screen.dart';
import 'package:flutter/material.dart';

class StudentItem extends StatelessWidget {
  final String id;
  final String lastName;
  final String firstName;
  final String imageUrl;
  final bool isDislodge;
  final bool isBack;

  StudentItem(
      this.id, this.lastName, this.firstName, this.imageUrl, this.isDislodge, this.isBack);

  @override
  Widget build(BuildContext context) {
    return InkResponse(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CheckUserScreen(id),
          ),
        );
      },
      child: GridTile(
        child: Image.network(
          imageUrl,
          fit: BoxFit.cover,
        ),
        footer: GridTileBar(
          backgroundColor: isDislodge ? Colors.grey : isBack ? Colors.blue : Colors.red,
          title: Text(
            lastName + " " + firstName,
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
