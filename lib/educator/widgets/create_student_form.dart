import 'dart:io';

import 'package:eternat/educator/providers/address_api_provider.dart';
import 'package:eternat/educator/screens/add_place_screen.dart';
import 'file:///D:/Cours-Helha/Bloc-3/Android/Flutter/Projet-Internat/eternat/lib/educator/utils/address_search.dart';
import 'package:eternat/educator/widgets/pickers/user_image_picker.dart';
import 'package:eternat/helpers/user_helper.dart';
import 'package:eternat/services/address_service.dart';
import 'package:eternat/services/firebaseauth_service.dart';
import 'package:eternat/services/firebasefirestore_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:gender_picker/gender_picker.dart';
import 'package:gender_picker/source/enums.dart';
import 'package:uuid/uuid.dart';

class CreateStudentForm extends StatefulWidget {
  @override
  _CreateStudentFormState createState() => _CreateStudentFormState();
}

class _CreateStudentFormState extends State<CreateStudentForm> {
  TextEditingController _emailController;
  TextEditingController _passwordController;
  TextEditingController _firstNameController;
  TextEditingController _lastNameController;
  final _addressController = TextEditingController();
  String _selectedGender =
      'homme'; //on déclare la valeur à homme car c'est le premier choix
  String _streetNumber = '';
  String _street = '';
  String _city = '';
  String _postalCode = '';
  String _dropdownValue = 'étudiant';
  var _userImageFile;
  DateTime _pickedDate;

  void _pickedImage(File image) {
    _userImageFile = image;
  }

  Future<Null> _pickDate(BuildContext context) async {
    final DateTime date = await showDatePicker(
      context: context,
      initialDate: _pickedDate,
      locale: const Locale('fr', 'FR'),
      firstDate: DateTime(DateTime.now().year - 99),
      lastDate: DateTime(DateTime.now().year + 5),
    );
    if (date != null && date != _pickedDate)
      setState(() {
        _pickedDate = date;
      });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _emailController = TextEditingController(text: "");
    _passwordController = TextEditingController(text: "");
    _firstNameController = TextEditingController(text: "");
    _lastNameController = TextEditingController(text: "");
    _pickedDate = DateTime.now();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(),
        Align(
          child: Column(
            children: [
              const SizedBox(height: 32.0),
              UserImagePicker(_pickedImage),
              const SizedBox(height: 10.0),
              TextField(
                controller: _emailController,
                decoration: InputDecoration(hintText: "Email"),
              ),
              const SizedBox(height: 10.0),
              TextField(
                controller: _passwordController,
                decoration: InputDecoration(hintText: "Mot de passe"),
              ),
              const SizedBox(height: 10.0),
              TextField(
                controller: _firstNameController,
                decoration: InputDecoration(hintText: "Prénom"),
              ),
              const SizedBox(height: 10.0),
              TextField(
                controller: _lastNameController,
                decoration: InputDecoration(hintText: "Nom de famille"),
              ),
              const SizedBox(height: 10.0),
              GenderPickerWithImage(
                maleText: "Homme",
                femaleText: "Femme",
                otherGenderText: "Autre",
                showOtherGender: true,
                selectedGender: Gender.Male,
                selectedGenderTextStyle: TextStyle(fontWeight: FontWeight.bold),
                unSelectedGenderTextStyle:
                    TextStyle(fontWeight: FontWeight.normal),
                onChanged: (Gender gender) {
                  if (gender == Gender.Female) {
                    _selectedGender = "femme";
                    print(_selectedGender);
                  } else if (gender == Gender.Male) {
                    _selectedGender = "homme";
                    print(_selectedGender);
                  } else {
                    _selectedGender = "autre";
                    print(_selectedGender);
                  }
                },
              ),
              const SizedBox(height: 10.0),
              DropdownButton<String>(
                value: _dropdownValue,
                icon: Icon(Icons.arrow_drop_down),
                iconSize: 24,
                elevation: 16,
                onChanged: (String newValue) {
                  setState(() {
                    _dropdownValue = newValue;
                  });
                },
                items: <String>['étudiant', 'éducateur']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
              const SizedBox(height: 10.0),
              ListTile(
                title: Text(
                    "Date de naissance : ${_pickedDate.year}/${_pickedDate.month}/${_pickedDate.day}"),
                trailing: Icon(Icons.date_range),
                onTap: () async {
                  _pickDate(context);
                },
              ),
              const SizedBox(height: 10.0),
              ListTile(
                title: Text(
                  "Adresse: Encore en construction, mais vous pouvez regarder l'avancement :). Veuillez utiliser la barre de recherche ci-dessous pour obtenir le bon résultat !",
                  style:
                      TextStyle(fontWeight: FontWeight.bold, color: Colors.red),
                ),
                trailing: Icon(Icons.map),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => AddPlaceScreen(),
                    ),
                  );
                },
              ),
              const SizedBox(height: 10.0),
              TextField(
                controller: _addressController,
                readOnly: true,
                onTap: () async {
                  //Génération d'un nouveau token.
                  final sessionToken = Uuid().v4();
                  final Suggestion result = await showSearch(
                    context: context,
                    delegate: AddressSearch(sessionToken),
                  );
                  // This will change the text displayed in the TextField
                  if (result != null) {
                    final addressDetails =
                        await AddressApiProvider(sessionToken)
                            .getPlaceDetailFromId(result.addressId);
                    setState(
                      () {
                        _addressController.text = result.description;
                        _streetNumber = addressDetails.streetNumber;
                        _street = addressDetails.street;
                        _city = addressDetails.city;
                        _postalCode = addressDetails.postalCode;
                      },
                    );
                  }
                },
                decoration: InputDecoration(
                  icon: Container(
                    width: 10,
                    height: 10,
                    child: Icon(
                      Icons.map,
                      color: Colors.black,
                    ),
                  ),
                  hintText: "Entrez votre adresse complète",
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.only(left: 8.0, top: 16.0),
                ),
              ),
              const SizedBox(height: 10.0),
              Text('N° de la rue: $_streetNumber'),
              Text('Rue: $_street'),
              Text('Ville: $_city'),
              Text('Code postal: $_postalCode'),
              const SizedBox(height: 10.0),
              RaisedButton(
                child: Text('Enregistrer'),
                onPressed: () async {
                  if (_userImageFile == null) {
                    Scaffold.of(context).showSnackBar(
                      SnackBar(
                        content: Text("S'il vous plait, prenez une image."),
                        backgroundColor: Theme.of(context).errorColor,
                      ),
                    );
                    return;
                  }
                  if (_emailController.text.isEmpty ||
                      _passwordController.text.isEmpty) {
                    print("Email and password cannot be empty");
                    return;
                  }
                  try {
                    User currentUser =
                        await FirebaseAuthService.saveCurrentUser();
                    final user =
                        await FirebaseAuthService.signupWithEmailAndPassword(
                      //trim() va retourner le string sans espace blanc devant et derrière.
                      email: _emailController.text.trim(),
                      password: _passwordController.text.trim(),
                    );
                    if (user != null) {
                      UserHelper.saveUser(
                        user,
                        _passwordController.text.trim(),
                        _dropdownValue.trim(),
                        _firstNameController.text.trim(),
                        _lastNameController.text.trim(),
                        _userImageFile,
                        _selectedGender.trim(),
                        _pickedDate,
                        _streetNumber.trim(),
                        _street.trim(),
                        _city.trim(),
                        _postalCode.trim(),
                      );
                      await FirebaseAuthService.reAuthenticateUser(currentUser);
                    }
                  } catch (e) {
                    print(e);
                  }
                },
              )
            ],
          ),
        ),
      ],
    );
  }
}
