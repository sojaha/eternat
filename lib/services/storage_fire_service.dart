import 'package:firebase_storage/firebase_storage.dart';

class StorageFireService {
  static FirebaseStorage _storage = FirebaseStorage.instance;

  //Méthode pour supprimer l'image du profil de la personne à supprimer.
  static Future deleteProfileImage(String uid) async {
    final storage = _storage.ref().child('image_profile').child(uid + '.jpg');
    await storage.delete();
  }
}