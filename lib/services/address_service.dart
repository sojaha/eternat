class Address {
  String streetNumber;
  String street;
  String city;
  String postalCode;

  Address({
    this.streetNumber,
    this.street,
    this.city,
    this.postalCode,
  });

  @override
  String toString() {
    return 'Place(streetNumber: $streetNumber, street: $street, city: $city, zipCode: $postalCode)';
  }
}

class Suggestion {
  final String addressId;
  final String description;

  Suggestion(this.addressId, this.description);

  @override
  String toString() {
    return 'Suggestion(description: $description, placeId: $addressId)';
  }
}