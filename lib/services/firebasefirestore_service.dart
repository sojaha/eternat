import 'package:cloud_firestore/cloud_firestore.dart';

class FirebaseFirestoreService {
  final String uid;
  FirebaseFirestoreService({this.uid});

  static final CollectionReference userCollection = FirebaseFirestore.instance.collection('users');

  static Future getSpecificUser(String profileID) {
    return userCollection.doc(profileID).get();
  }

  Future<String> getSpecificPasswordUser() async {
    var user = await getSpecificUser(uid);
    final data = user.data();
    return data['password'];
  }

  Future deleteUser() {
    return userCollection.doc(uid).delete();
  }

  Future isEducator() async {
    var user = await userCollection.doc(uid).get();
    final data = user.data();
    if (data['role'] == 'éducateur') {
      return true;
    }
  }
}