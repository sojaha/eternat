import 'package:eternat/helpers/user_helper.dart';
import 'package:eternat/services/firebasefirestore_service.dart';
import 'package:eternat/services/storage_fire_service.dart';
import 'package:firebase_auth/firebase_auth.dart';

class FirebaseAuthService {
  static FirebaseAuth auth = FirebaseAuth.instance;

  static signInWithEmailAndPassword({String email, String password}) async {
    final res = await auth.signInWithEmailAndPassword(
        email: email, password: password);
    final User user = res.user;
    return user;
  }

  static reAuthenticateUser(User currentUser) async {
    String educatorEncodedPassword = await FirebaseFirestoreService(uid: currentUser.uid).getSpecificPasswordUser();
    print(educatorEncodedPassword);
    String educatorDecodedPassword = UserHelper.decodePassword(educatorEncodedPassword);
    currentUser = await signInWithEmailAndPassword(email: currentUser.email, password: educatorDecodedPassword);
    AuthCredential educatorCredential = EmailAuthProvider.credential(email: currentUser.email, password: educatorDecodedPassword);
    UserCredential educatorResult = await currentUser.reauthenticateWithCredential(educatorCredential);
    print(educatorResult.user.uid);
    return educatorResult;
  }

  static signupWithEmailAndPassword({String email, String password}) async {
    final res = await auth.createUserWithEmailAndPassword(
        email: email, password: password);
    final User user = res.user;
    return user;
  }

  static logOut() {
    return auth.signOut();
  }

   static saveCurrentUser() async {
    User actualUser = auth.currentUser;
    print(actualUser.uid);
    String educatorEncodedPassword = await FirebaseFirestoreService(uid: actualUser.uid).getSpecificPasswordUser();
    print(educatorEncodedPassword);
    String educatorDecodedPassword = UserHelper.decodePassword(educatorEncodedPassword);
    print(educatorDecodedPassword);
    return actualUser;
  }

  static deleteUser(String email, String encodedPassword) async {
    try {
      User saveUser = await saveCurrentUser();
      //PREMIERE PARTIE
      String decodedPassword = UserHelper.decodePassword(encodedPassword);
      User user = await signInWithEmailAndPassword(email: email, password: decodedPassword);
      AuthCredential credential = EmailAuthProvider.credential(email: email, password: decodedPassword);
      //AuthResult a changé en UserCredential
      UserCredential result = await user.reauthenticateWithCredential(credential);
      await FirebaseFirestoreService(uid: result.user.uid).deleteUser();
      await StorageFireService.deleteProfileImage(result.user.uid);
      await result.user.delete();
      return reAuthenticateUser(saveUser);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
