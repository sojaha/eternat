import 'package:eternat/authentication/auth/screens/auth_screen.dart';
import 'package:eternat/educator/educator_main.dart';
import 'package:eternat/services/firebaseauth_service.dart';
import 'package:eternat/services/firebasefirestore_service.dart';
import 'package:eternat/student/screens/student_home_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class AuthenticationService {
  static connection(BuildContext context) async {
    Route route;
    if (FirebaseAuthService.auth.currentUser != null &&
        await FirebaseFirestoreService(uid: FirebaseAuthService.auth.currentUser.uid)
            .isEducator()) {
      FirebaseAuthService.auth.authStateChanges().listen((User user) {
        if (user != null) {
          route = MaterialPageRoute(builder: (ctx) => EducatorMain());
          Navigator.push(context, route);
        } else {
          route = MaterialPageRoute(builder: (ctx) => AuthScreen());
          Navigator.push(context, route);
        }
      });
    } else if (FirebaseAuthService.auth.currentUser != null) {
      FirebaseAuthService.auth.authStateChanges().listen((User user) {
        if (user != null) {
          route = MaterialPageRoute(builder: (ctx) => StudentHomeScreen());
          Navigator.push(context, route);
        } else {
          route = MaterialPageRoute(builder: (ctx) => AuthScreen());
          Navigator.push(context, route);
        }
      });
    } else {
      route = MaterialPageRoute(builder: (ctx) => AuthScreen());
      Navigator.push(context, route);
    }
  }
}
