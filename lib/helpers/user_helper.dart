import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';

class UserHelper {
  static FirebaseFirestore _db = FirebaseFirestore.instance;
  static FirebaseStorage _storage = FirebaseStorage.instance;

  static saveUser(User user, String password, String role, String firstName, String lastName,
      File profileImage, String gender,DateTime birthday, String streetNumber, String street, String city, String postalCode) async {
    Map<String, dynamic> address = {
      "numéro": streetNumber,
      "rue": street,
      "ville": city,
      "code_postal": postalCode,
    };

    Map<String, dynamic> userData = {
      "id": user.uid,
      "email": user.email,
      "password": encodePassword(password),
      "prénom": firstName,
      "nom": lastName,
      "imageUrl": await storeProfileImage(user, profileImage),
      "sexe": gender,
      "isDislodge": true,
      "isBack": false,
      "last_login": user.metadata.lastSignInTime.millisecondsSinceEpoch,
      "created_at": user.metadata.creationTime.millisecondsSinceEpoch,
      "role": role,
      "date_de_naissance": birthday,
      "adresse": address,
    };
    await _db.collection("users").doc(user.uid).set(userData);
  }

  static Future<String> storeProfileImage(User user, File profileImage) async {
    final storage = _storage.ref().child('image_profile').child(
        user.uid + '.jpg');
    await storage.putFile(profileImage);
    return await storage.getDownloadURL();
  }

  static String encodePassword(String password) {
    String encoded = base64.encode(utf8.encode(password));
    return encoded;
  }

  //ATTENTION A LA FAILLE DE SECURITE
  static String decodePassword(String password) {
    String encoded = utf8.decode(base64.decode(password));
    return encoded;
  }

}
