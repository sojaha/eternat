import 'dart:convert';

import 'package:http/http.dart' as http;

const GOOGLE_API_KEY = 'AIzaSyAdIHdp-e6Nn9MbIP9K4NqE0rcyR1XIbEc';

class LocationHelper {
  static String generateLocationPreviewImage({double latitude, double longitude}) {
    return 'https://maps.googleapis.com/maps/api/staticmap?center=&$latitude,$longitude,&zoom=13&size=600x300&maptype=roadmap&markers=color:red%7Clabel:C%7C$latitude,$longitude&key=$GOOGLE_API_KEY';
  }

  //On va essayer d'envoyer une requête HTTP vers google pour pouvoir récupérer l'adresse de notre marqueur
  //On veut faire une demande de géocodage qui va nous permettre de traduire les adresses en coordonnées.
  //Moi je veux faire une demande de géocodage inverse: Une recherche d'adresse !
  //https://developers.google.com/maps/documentation/geocoding/overview#ReverseGeocoding
  static Future<String> getPlaceAddress(double lat, double lng) async {
    //on va préparer l'url
    final url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&key=$GOOGLE_API_KEY';
    //Ensuite on va devoir encoyer une demande d'accès à cette adresse
    final response = await http.get(url);
    //Comme les données sont en format json, on va devoir les convertir
    return json.decode(response.body)['results'][0]['formatted_address']; //On a une clé de résulats,
    // le 0 pour accéder à la première entrée que Google nous renvoie car elle pourrait trouver plusieurs adresses mais les classera par ordre de pertinence
    // Et enfin, on retrouve un champs d'adresse formattée qui devrait être l'adresse lisible par nous
  }
}